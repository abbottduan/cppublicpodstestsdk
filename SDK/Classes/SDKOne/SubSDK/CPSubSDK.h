//
//  CPSubSDK.h
//  CPPublicPodsTestSDK
//
//  Created by duansong on 2018/6/20.
//  Copyright © 2018 LinkSure. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CPSubSDK : NSObject

- (void)subSDK;

- (void)subSDKV3;

@end
